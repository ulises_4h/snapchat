package dinguskhan.social_media_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID ="96556015-C0B5-36DE-FF7C-9A94F97D7E00";
    public static final String SECRET_KEY ="B082079E-2D1E-25BF-FF8E-906136531F00";
    public static final String VERSION ="v1";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }else{
            LoggedInFragment loggedIn = new LoggedInFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loggedIn).commit();
        }
    }
}
