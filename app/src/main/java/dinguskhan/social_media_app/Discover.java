package dinguskhan.social_media_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Discover extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DiscoverFragment discover = new DiscoverFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, discover).commit();
    }
}
